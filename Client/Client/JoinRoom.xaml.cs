﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Window
    {
        public JoinRoom()
        {
            InitializeComponent();
            backButton.Click += delegate { App.BackToMain(); };
            Closing += App.Window_Closing;
            chosenRoomID = -1;

            ShowRoomList();
        }

        private Label CreateNewRoomLabel(string content)
        {
            Label label = new Label
            {
                FontSize = 24,
                Width = scrollViewerRooms.Width,
                Height = double.NaN,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontFamily = new FontFamily("Arial"),
                FontWeight = FontWeights.Bold,
                Content = content
            };
            return label;
        }

        private Label CreateNewPlayerLabel(string content)
        {
            Label label = new Label
            {
                FontSize = 16,
                Width = scrollViewerPlayers.Width,
                Height = double.NaN,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontFamily = new FontFamily("Arial"),
                FontWeight = FontWeights.Bold,
                Content = content
            };
            return label;
        }

        private void ShowRoomList()
        {
            GetRoomsResult getRoomsResult = App.GetClient().GetRooms();
            if (getRoomsResult.success)
            {
                roomLabels = new Dictionary<int, Label>();
                foreach (KeyValuePair<int, string> roomData in getRoomsResult.rooms)
                {
                    Label roomLabel = CreateNewRoomLabel(roomData.Value);
                    int roomId = roomData.Key; // must do this for the delegate.
                    roomLabel.MouseLeftButtonUp += delegate { ShowRoomPlayers(roomId); };
                    roomLabels[roomId] = roomLabel;
                    scrollPanelRooms.Children.Add(roomLabel);
                }
            }
        }

        private void ClearRoomList()
        {
            scrollPanelRooms.Children.Clear();
        }
        private void ClearPlayersList()
        {
            playersCanvas.Visibility = Visibility.Collapsed;
            scrollPanelPlayers.Children.Clear();
            chosenRoomID = -1;
        }

        private void RefreshRoomsOnClick(object o, EventArgs e)
        {
            errorMessage.Text = "";
            ClearRoomList();
            ClearPlayersList();
            ShowRoomList();
        }

        private void JoinRoomOnClick(object o, EventArgs e)
        {
            string roomName = null;
            if(roomLabels.ContainsKey(chosenRoomID))
            {
                roomName = Convert.ToString(roomLabels[chosenRoomID].Content);
            }

            JoinRoomResult joinRoomResult = App.GetClient().JoinRoom(chosenRoomID, roomName);
            if (joinRoomResult.success)
            {
                App.ShowWindow(new WaitForGame(false));
                App.closeAllWindows = false;
                this.Close();
            }
            else
            {
                errorMessage.Text = joinRoomResult.errorMessage;
            }
        }

        private void ShowRoomPlayers(int roomId)
        {
            if(chosenRoomID != -1)
            {
                roomLabels[chosenRoomID].Background = Brushes.White;
                ClearPlayersList();
                errorMessage.Text = "";
            }
            chosenRoomID = roomId;
            roomLabels[chosenRoomID].Background = Brushes.Blue;

            GetPlayersInRoomResult getPlayersResult = App.GetClient().GetPlayersInRoom(chosenRoomID);

            if (getPlayersResult.success)
            {
                playersCanvas.Visibility = Visibility.Visible;
                foreach (string playerName in getPlayersResult.players)
                {
                    Label playerLabel = CreateNewPlayerLabel(playerName);
                    scrollPanelPlayers.Children.Add(playerLabel);
                }
            }
            else
            {
                errorMessage.Text = getPlayersResult.errorMessage;
            }
        }

        private Dictionary<int, Label> roomLabels;
        private int chosenRoomID;
    }
}
