﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
            backButton.Click += delegate { App.BackToMain(); };
            this.Closing += App.Window_Closing;
        }

        private void Input_TextChanged(object sender, TextChangedEventArgs e)
        {
            errorMessage.Text = "";
        }

        public void SignupOnClick(object o, EventArgs e)
        {
            SignupResult result = App.GetClient().SignUp(inputUser.Text, inputPass.Text, inputEmail.Text);

            if (result.success)
            {
                App.ShowMainWindow();
                App.closeAllWindows = false;
                this.Close();
            }
            else
            {
                errorMessage.Text = result.errorMessage;
            }
        }
    }
}
