﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            menuButtons = new Button[] { joinRoomButton, createRoomButton, bestScoresButton, myStatusButton };
            SetMenuButtonsActive(false);
        }

        public void OnShowing(object o, EventArgs e)
        {
            errorMessage.Text = "";
        }
        
        public void QuitOnClick(object o, EventArgs e)
        {
            Close();
            Application.Current.Shutdown();
        }
        public void InputChange(object o, EventArgs e)
        {
            errorMessage.Text = "";
        }
    
        public void SignInOnClick(object o, EventArgs e)
        {
            SigninResult result = App.GetClient().SignIn(inputUser.Text, inputPass.Password);

            if (result.success)
            {
                helloUserLabel.Content = helloUserLabel.Content.ToString().Replace("{[name]}", inputUser.Text);
                helloUserLabel.Visibility = Visibility.Visible;
                loginCanvas.Visibility = Visibility.Collapsed;
                logo.Visibility = Visibility.Collapsed;

                signOutButton.Visibility = Visibility.Visible;
                signUpButton.Visibility = Visibility.Collapsed;

                SetMenuButtonsActive(true);
            }
            else
            {
                errorMessage.Text = result.errorMessage;
            }
        }

        public void SignUpOnClick(object o, EventArgs e)
        {
            App.ShowWindow<SignUpWindow>();
        }
        public void ShowStatus(object o, EventArgs e)
        {
            App.ShowWindow<MyStatus>();
        }
        public void CreateRoomOnClick(object o, EventArgs e)
        {
            App.ShowWindow<CreateRoom>();
        }
        public void ShowBestScores(object o, EventArgs e)
        {
            App.ShowWindow<BestScores>();
        }
        public void JoinRoomOnClick(object o, EventArgs e)
        {
            App.ShowWindow<JoinRoom>();
        }

        public void SignOutOnClick(object o, EventArgs e)
        {
            loginCanvas.Visibility = Visibility.Visible;
            logo.Visibility = Visibility.Visible;
            helloUserLabel.Content = "Hello {[name]}";
            helloUserLabel.Visibility = Visibility.Collapsed;

            signUpButton.Visibility = Visibility.Visible;
            signOutButton.Visibility = Visibility.Collapsed;

            SetMenuButtonsActive(false);

            //Resets login details
            inputUser.Text = "";
            inputPass.Password = "";

            App.GetClient().Signout();
        }

        private void SetMenuButtonsActive(bool active)
        {
            for (int i = 0; i < menuButtons.Length; i++)
            {
                menuButtons[i].IsEnabled = active;
            }
        }

        private Button[] menuButtons;
    }
}
