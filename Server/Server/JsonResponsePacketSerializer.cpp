#include "JsonResponsePacketSerializer.h"
#include "ResponseCode.h"

char * JsonResponsePacketSerializer::serializeResponse(ErrorResponse res)
{
	json j;
	
	j["message"] = res.message;
	
	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(LoginResponse res)
{	
	json j;
	
	j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));
	
	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(SignUpResponse res)
{
	json j;

	j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse res)
{
	json j;
	
	j["roomCount"] = res.rooms.size();
	
	int i = 0;
	for (auto& room : res.rooms)
	{
		j[std::to_string(i++)] = std::to_string(room.first) + "," + room.second;
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse res)
{
	json j;

	if (res.status == ResponseCode::Success)
	{
		j["playersCount"] = res.players.size();

		for (int i = 0; i < res.players.size(); ++i)
		{
			j[std::to_string(i)] = res.players[i].getUsername();
		}
	}
	else
	{
		j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));
	}
	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse res)
{
	json j;

	j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(JoinLeaveRoomUpdateResponse res)
{
	json j;

	if (res.status == ResponseCode::Success)
	{
		j["playerName"] = res.playerName;
	}
	else
	{
		j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(KickPlayerResponse res)
{
	json j;

	if (res.status == ResponseCode::Success)
	{
		j["playerName"] = res.playerName;
	}
	else
	{
		j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(NewMessageResponse res)
{
	json j;

	if (res.status == ResponseCode::Success)
	{
		j["playerName"] = res.playerName;
		j["message"] = res.message;
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse res)
{
	json j;

	j["message"] = ResponseMessages::getMessage(ResponseCode(res.status));

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(HighscoreResponse res)
{
	json j;

	j["usersCount"] = res.highscores.size();

	for (int i = 0; i < res.highscores.size(); i++)
	{
		j["score" + std::to_string(i)] = res.highscores[i].score;
		j["username" + std::to_string(i)] = res.highscores[i].username;
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(GetPerformancesResponse res)
{
	json j;

	j["gamesCount"] = res.gamesCount;
	j["rightAnswersCount"] = res.rightAnswersCount;
	j["wrongAnswersCount"] = res.wrongAnswersCount;
	j["averageTimeForAnswer"] = res.averageTimeForAnswer;

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse res)
{
	json j;

	j["roomName"] = res.roomData.name;
	j["answerTimeout"] = res.roomData.timePerQuestion;
	j["maxPlayersCount"] = res.roomData.maxPlayers;
	j["questionCount"] = res.roomData.questionCount;

	j["playersCount"] = res.players.size();
	for (int i = 0; i < res.players.size(); ++i)
	{
		j[std::to_string(i)] = res.players[i];
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse res)
{
	json j;

	if (!res.question.empty())
	{
		j["question"] = res.question;
		for (int i = 0; i < res.answers.size(); i++)
		{
			j[std::to_string(i)] = res.answers[i];
		}
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse res)
{
	json j;

	j["correctAnswer"] = res.correctAnswer;

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse res)
{
	json j;

	int i = 0;
	j["playersCount"] = res.results.size();
	for (vector<PlayerResults>::iterator playerRes = res.results.begin(); playerRes != res.results.end(); ++playerRes)
	{
		j[std::to_string(i++)] = playerRes->username + "," + std::to_string(playerRes->correctAnswerCount);
	}

	return jsonToCharArray(j);
}

char * JsonResponsePacketSerializer::jsonToCharArray(json j)
{
	std::string serializedJson = j.dump();
	char *buff = new char[serializedJson.length() + 1];
	strcpy(buff, serializedJson.c_str());
	buff[serializedJson.length()] = 0;
	
	return buff;
}
