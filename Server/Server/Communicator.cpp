#include "Communicator.h"
#include "ResponseCode.h"
#include <string>

#pragma warning(disable:4996)

#define FORCED_SOCKET_CLOSE 10054
#define SOCKET_CLOSED 10038

extern const int CODE_ID_SIZE = 3;
extern const int DATA_LEN_SIZE = 4;

using std::thread;
using std::map;

Communicator::Communicator(RequestHandlerFactory *handlerFactory) : m_handlerFactory(handlerFactory)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}

void Communicator::bindAndListen(int port)
{
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	//sa.sin_addr.s_addr = INADDR_ANY;
	sa.sin_addr.s_addr = inet_addr("192.168.43.128");    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while(true)
	{
		try
		{
			SOCKADDR_IN client_info = { 0 };
			int addrsize = sizeof(client_info);

			SOCKET client_socket = ::accept(m_serverSocket, (struct sockaddr*)&client_info, &addrsize);
			if (client_socket == INVALID_SOCKET)
				throw std::exception(__FUNCTION__);

			char *ip = inet_ntoa(client_info.sin_addr);
			startThreadForNewClient(ip, client_socket);
		}
		catch (std::exception e)
		{
			std::cout << e.what() << std::endl;
		}
	}
	
}

void Communicator::startThreadForNewClient(char *ip, SOCKET client_socket)
{
	std::cout << "New connection from " << ip << " " << client_socket << std::endl;

	m_clientThreads.push_back(std::move(thread(&Communicator::handleRequests, this, ip, client_socket)));
}

void Communicator::sendError(SOCKET client_socket)
{
	ErrorResponse error = { ResponseMessages::getMessage(ResponseCode::Error) };
	char *responseJson = JsonResponsePacketSerializer::serializeResponse(error);
	char *responseBuffer = createResponse(ResponseCode::Error, responseJson);
	send(client_socket, responseBuffer, strlen(responseBuffer), NULL);
	delete responseBuffer;
}

char *Communicator::createResponse(int status, char *serializedRes)
{
	char *response = new char[CODE_ID_SIZE + DATA_LEN_SIZE + strlen(serializedRes) + 1]{ 0 };

	std::stringstream buff;
	buff << std::setw(CODE_ID_SIZE) << std::setfill('0') << status;
	strcat(response, buff.str().c_str());

	// clear buff.
	buff.str("");
	buff.clear();

	buff << std::setw(DATA_LEN_SIZE) << std::setfill('0') << strlen(serializedRes);
	strcat(response, buff.str().c_str());

	strcat(response, serializedRes);

	return response;
}

char *Communicator::createResponse(int status)
{
	char *response = new char[CODE_ID_SIZE + DATA_LEN_SIZE + 1]{ 0 };

	std::stringstream buff;
	buff << std::setw(CODE_ID_SIZE) << std::setfill('0') << status;
	strcat(response, buff.str().c_str());

	// clear buff.
	buff.str("");
	buff.clear();

	buff << std::setw(DATA_LEN_SIZE) << std::setfill('0') << 0;
	strcat(response, buff.str().c_str());

	return response;
}

void Communicator::updateUsersInRoom(RoomChangeRequestUpdate *update)
{
	switch (update->updateRoomType)
	{
		case ResponseCode::RoomPlayerLeaveJoin: 
			joinOrLeaveUpdate((JoinLeaveRequestUpdate*)update);
			break;
		case ResponseCode::RoomClosed: 
			closeRoomUpdate((RoomClosedRequestUpdate*)update);
			break;
		case ResponseCode::GameStarted:
			startGameUpdate((StartGameRequestUpdate*)update);
			break; 
		case ResponseCode::RoomKickPlayer:
			kickPlayerUpdate((KickPlayerRequestUpdate*)update);
			break;
		case ResponseCode::NewMessageUpdate:
			newMessageUpdate((NewMessageRequestUpdate*)update);
			break;
	}
}

void Communicator::joinOrLeaveUpdate(JoinLeaveRequestUpdate *update)
{
	char *updateMsg = createResponse(ResponseCode::RoomPlayerLeaveJoin, JsonResponsePacketSerializer::serializeResponse(JoinLeaveRoomUpdateResponse{ ResponseCode::Success, update->playerName }));
	for (int i = 0; i < update->sockets.size(); i++)
	{
		send(update->sockets[i], updateMsg, strlen(updateMsg), NULL);
	}
}

void Communicator::startGameUpdate(StartGameRequestUpdate *update)
{
	char *updateMsg = createResponse(ResponseCode::GameStarted);
	for (int i = 0; i < update->sockets.size(); i++)
	{
		m_clients[update->sockets[i]] = m_handlerFactory->createGameRequestHandler(LoggedUser(update->users[i]), update->gameId);
		send(update->sockets[i], updateMsg, strlen(updateMsg), NULL);
	}
}

void Communicator::closeRoomUpdate(RoomClosedRequestUpdate *update)
{
	char *updateMsg = createResponse(ResponseCode::RoomClosed);
	for (int i = 0; i < update->sockets.size(); i++)
	{
		m_clients[update->sockets[i]] = m_handlerFactory->createMenuRequestHandler(update->users[i]);
		send(update->sockets[i], updateMsg, strlen(updateMsg), NULL);
	}
}

void Communicator::kickPlayerUpdate(KickPlayerRequestUpdate *update)
{
	m_clients[update->playerSocket] = m_handlerFactory->createMenuRequestHandler(LoggedUser(update->playerName));
	
	char *updateMsg = createResponse(ResponseCode::RoomKickPlayer, JsonResponsePacketSerializer::serializeResponse(KickPlayerResponse{ ResponseCode::Success, update->playerName }));
	for (int i = 0; i < update->sockets.size(); i++)
	{
		send(update->sockets[i], updateMsg, strlen(updateMsg), NULL);
	}
}

void Communicator::newMessageUpdate(NewMessageRequestUpdate *update)
{
	char *updateMsg = createResponse(ResponseCode::NewMessageUpdate, JsonResponsePacketSerializer::serializeResponse(NewMessageResponse{ ResponseCode::Success, update->playerName, update->message }));
	for (int i = 0; i < update->sockets.size(); i++)
	{
		send(update->sockets[i], updateMsg, strlen(updateMsg), NULL);
	}
}

void Communicator::handleRequests(char *ip, SOCKET client_socket)
{
	m_clients[client_socket] = m_handlerFactory->createLoginRequestHandler();
	Request request;
	char *buff;
	int dataLen;

	while (m_clients[client_socket])
	{
		// get request code.
		buff = new char[CODE_ID_SIZE + 1];

		if (!recv(client_socket, buff, CODE_ID_SIZE, NULL) || WSAGetLastError() == FORCED_SOCKET_CLOSE || WSAGetLastError() == SOCKET_CLOSED)
		{
			delete buff;
			break;
		}
		request.id = atoi(buff);
		delete buff;

		// get data length.
		buff = new char[DATA_LEN_SIZE + 1];
		if (!recv(client_socket, buff, DATA_LEN_SIZE, NULL) || WSAGetLastError() == FORCED_SOCKET_CLOSE || WSAGetLastError() == SOCKET_CLOSED)
		{
			delete buff;
			break;
		}
		buff[DATA_LEN_SIZE] = 0;
		dataLen = atoi(buff);
		delete buff;

		// get data.
		if (dataLen)
		{
			buff = new char[dataLen + 1];
			if (!recv(client_socket, buff, dataLen, NULL) || WSAGetLastError() == FORCED_SOCKET_CLOSE || WSAGetLastError() == SOCKET_CLOSED)
			{
				delete buff;
				break;
			}
			buff[dataLen] = 0;
			request.buffer = buff;
		}
		else
		{
			request.buffer = NULL;
		}

		// get time.
		request.receivalTime = time(NULL);

		if (m_clients[client_socket]->isRequestRelevant(request))
		{
			try
			{
				RequestResult *result = m_clients[client_socket]->handleRequest(request);
			
				if (result->response)
				{
					send(client_socket, result->response, strlen(result->response), NULL);
					delete result->response;
				}

				if (dynamic_cast<RoomChangeRequestUpdate*>(result))
				{
					updateUsersInRoom((RoomChangeRequestUpdate*)result);
				}

				if (m_clients[client_socket] != result->newHandler)
				{
					bool wasInMenu = typeid(*m_clients[client_socket]) == typeid(MenuRequestHandler);
					
					delete m_clients[client_socket];
					m_clients[client_socket] = result->newHandler;
					
					if (wasInMenu && typeid(*m_clients[client_socket]) != typeid(LoginRequestHandler))
					{
						((RoomMemberRequestHandler*)m_clients[client_socket])->getRoom()->addSocket(((RoomMemberRequestHandler*)m_clients[client_socket])->getUser().getUsername(), client_socket);
					}
				}
				delete result;
			}
			catch (std::exception e)
			{
				std::cout << e.what() << std::endl;
				sendError(client_socket);
			}
			catch (...)
			{
				sendError(client_socket);
			}
		}
		else
		{
			sendError(client_socket);
		}
		delete request.buffer;
	}

	if (m_clients[client_socket] && typeid(*m_clients[client_socket]) != typeid(LoginRequestHandler))
	{
		m_clients[client_socket]->forceSignout();

		if (typeid(*m_clients[client_socket]) == typeid(RoomMemberRequestHandler)) // m_clients[client_socket] of type RoomAdminRequestHandler return false here.
		{
			JoinLeaveRequestUpdate* leaveRoomUpdate = new JoinLeaveRequestUpdate;
			leaveRoomUpdate->updateRoomType = ResponseCode::RoomPlayerLeaveJoin;
			leaveRoomUpdate->sockets = ((RoomMemberRequestHandler*)m_clients[client_socket])->getRoom()->getAllSockets();
			leaveRoomUpdate->playerName = ((RoomMemberRequestHandler*)m_clients[client_socket])->getUser().getUsername();
			updateUsersInRoom(leaveRoomUpdate);
			delete leaveRoomUpdate;
		}
		else if (typeid(*m_clients[client_socket]) == typeid(RoomAdminRequestHandler))
		{
			RoomClosedRequestUpdate* closeRoomUpdate = new RoomClosedRequestUpdate;
			closeRoomUpdate->updateRoomType = ResponseCode::RoomClosed;
			closeRoomUpdate->sockets = ((RoomMemberRequestHandler*)m_clients[client_socket])->getRoom()->getAllSockets();
			closeRoomUpdate->users = ((RoomMemberRequestHandler*)m_clients[client_socket])->getRoom()->getAllUsers();
			updateUsersInRoom(closeRoomUpdate);
			((RoomAdminRequestHandler*)m_clients[client_socket])->closeRoom();
			delete closeRoomUpdate;
		}
	}
	std::cout << ip << " " << client_socket << " Disconnected!" << std::endl;
	m_clients.erase(client_socket);
	closesocket(client_socket);
}