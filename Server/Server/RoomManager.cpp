#include "RoomManager.h"

const float RoomManager::MIN_AMSWER_TIMEOUT = 2.0;

RoomManager::RoomManager(IDataBase *db) : m_nextRoomId(0), m_database(db)
{
}

RoomManager *RoomManager::m_object = NULL;
RoomManager *RoomManager::createObject(IDataBase *db)
{
	if (m_object)
	{
		throw std::exception("RoomManager object has already been created");
	}

	m_object = new RoomManager(db);
	return m_object;
}
RoomManager * RoomManager::getObject()
{
	if (!m_object)
	{
		throw std::exception("RoomManager object must be created first.");
	}
	return m_object;
}

Room *RoomManager::createRoom(LoggedUser &user, CreateRoomRequest request)
{
	if (!isalpha(request.roomName[0]) || request.roomName.size() < MIN_ROOM_NAME_LEN) throw ResponseCode::InvalidCreateRoomRoomName;
	if (request.maxUsers < MIN_USERS) throw ResponseCode::InvalidCreateRoomMaxUsers;
	if (request.questionCount < MIN_QUESTION_COUNT || request.questionCount > m_database->getQuestionsCount()) throw ResponseCode::InvalidCreateRoomQuestionCount;
	if (request.answerTimeout < MIN_AMSWER_TIMEOUT) throw ResponseCode::InvalidCreateRoomAnswerTimeout;

	m_rooms[m_nextRoomId] = Room(RoomData{ m_nextRoomId, request.roomName, request.maxUsers, request.questionCount, request.answerTimeout, false });
	m_rooms[m_nextRoomId].addUser(user);

	return &m_rooms[m_nextRoomId++];
}

void RoomManager::deleteRoom(int ID)
{
	m_rooms.erase(ID);
}

bool RoomManager::getRoomState(int ID)
{
	return m_rooms[ID].getMetadata().isActive;
}

map<int,Room> RoomManager::getRooms()
{
	return m_rooms;
}

Room *RoomManager::getRoom(int ID)
{
	return &m_rooms.at(ID);
}
