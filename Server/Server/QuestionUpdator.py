import sqlite3
from sqlite3 import Error
from collections import namedtuple
Question = namedtuple("Question", "question ans1 ans2 ans3 ans4 correctAns")

def getQuestions(filepath):
    qFile = open(filepath, "r")
    questionsTxt = qFile.readlines()
    questionList = []
    for questionTxt in questionsTxt:
        questionParts = questionTxt.split('|')
        questionList.append(Question(questionParts[0], questionParts[1], questionParts[2], questionParts[3], questionParts[4], questionParts[5]))

    return questionList

def insertQuestions(sqlHandler ,questions):
    for question in questions:
        question : Question
        sqlHandler.execute("INSERT INTO QUESTIONS(question, answer0, answer1, answer2, answer3, correctAnswer) VALUES(\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", {5});".format(question.question, question.ans1, question.ans2, question.ans3, question.ans4, question.correctAns))

def clearQuestionsTable(sqlHandler):
    sqlHandler.execute("DELETE FROM QUESTIONS;")

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        sqlHandler = sqlite3.connect(db_file)
        return sqlHandler
    except Error as e:
        print(e)

if __name__ == '__main__':
    sqlHandler = create_connection("triviaDB.sqlite")

    clearQuestionsTable(sqlHandler)
    insertQuestions(sqlHandler, getQuestions("questions.txt"))

    sqlHandler.commit()
    sqlHandler.close()
