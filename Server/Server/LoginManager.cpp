#include "LoginManager.h"
#include <algorithm>
#include <regex>

LoginManager::LoginManager(IDataBase *db) : m_database(db) { }

LoginManager *LoginManager::m_object = NULL;
LoginManager *LoginManager::createObject(IDataBase *db)
{
	if (m_object)
	{
		throw std::exception("LoginManager object has already been created");
	}

	m_object = new LoginManager(db);
	return m_object;
}
LoginManager * LoginManager::getObject()
{
	if (!m_object)
	{
		throw std::exception("LoginManager object must be created first.");
	}
	return m_object;
}

ResponseCode LoginManager::signup(string username, string email, string password)
{
	if (!isValidEmail(email)) return ResponseCode::InvalidEmail;
	if (!isValidUsername(username)) return ResponseCode::InvalidUsername;
	if (!isValidPassword(password)) return ResponseCode::InvalidPassword;
	
	try
	{
		if (m_database->doesEmailTaken(email)) return ResponseCode::EmailTaken;
		if (m_database->doesUsernameTaken(username)) return ResponseCode::UserTaken;
		
		m_database->addUser(username, email, password);
	} 
	catch(SqlException &e)
	{
		return ResponseCode::SqlInjection;
	}

	return ResponseCode::Success;
}

ResponseCode LoginManager::login(string username, string password)
{
	if (isLoggedIn(username)) return ResponseCode::AlreadyLoggedIn;
	try
	{
		if (!m_database->login(username, password))	return ResponseCode::InvalidPasswordAndUsername;
	}
	catch (SqlException &e)
	{
		return ResponseCode::SqlInjection;
	}

	m_loggedUsers.push_back(LoggedUser(username));
	return ResponseCode::Success;

}

bool LoginManager::isLoggedIn(string username)
{
	for (vector<LoggedUser>::iterator user = m_loggedUsers.begin(); user != m_loggedUsers.end(); user++)
	{
		if (user->getUsername() == username)
		{
			return true;
		}
	}
	return false;
}

void LoginManager::logout(string username)
{
	for (vector<LoggedUser>::iterator user = m_loggedUsers.begin(); user != m_loggedUsers.end();)
	{
		if (user->getUsername() == username)
		{
			user = m_loggedUsers.erase(user);
		}
		else
		{
			user++;
		}
	}
}

bool LoginManager::isValidUsername(string username)
{
	return isalpha(username[0]) && username.size() >= USER_MIN_LEN;
}

bool LoginManager::isValidPassword(string password)
{
	bool hasCapital = std::find_if(password.begin(), password.end(), [](char c) { return isupper(c); }) != password.end();
	bool hasLetter = std::find_if(password.begin(), password.end(), [](char c) { return isalpha(c); }) != password.end();
	bool hasDigits = std::find_if(password.begin(), password.end(), [](char c) { return isdigit(c); }) != password.end();

	for (int i = 0; i < password.size() && !(hasCapital && hasLetter && hasDigits); ++i)
	{
		if (isupper(password[i]))
		{
			hasCapital = true;
		}

		else if (isalpha(password[i]))
		{
			hasLetter = true;
		}

		else if (isdigit(password[i]))
		{
			hasDigits = true;
		}
	}

	return hasCapital && hasLetter && hasDigits && password.size() >= PASSWORD_MIN_LEN;
}

bool LoginManager::isValidEmail(string email)
{
	// define a regular expression
	const std::regex pattern
	("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");

	// try to match the string with the regular expression
	return std::regex_match(email, pattern);
}