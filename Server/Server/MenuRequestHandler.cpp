#include "MenuRequestHandler.h"
#include "Communicator.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory *handlerFactory, LoginManager *loginManager, RoomManager *roomManager, LoggedUser user, HighscoreTable *highscoreTable, IDataBase *database) : m_handlerFactory(handlerFactory), m_loginManager(loginManager), m_roomManager(roomManager), m_user(user), m_highscoreTable(highscoreTable), m_database(database)
{
}

MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(Request request)
{
	switch (request.id)
	{
		case RequestCode::Logout:
		case RequestCode::MyPerformances:
		case RequestCode::GetRooms:
		case RequestCode::BestScores:
			return true;
		case RequestCode::CreateRoom:
		case RequestCode::GetPlayersInRoom:
		case RequestCode::JoinRoom:
			
			return request.buffer != NULL;
		
		default:
			return false;
	}
}

RequestResult *MenuRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
		case RequestCode::Logout: return signout();
		case RequestCode::GetRooms: return getRooms(request);
		case RequestCode::CreateRoom: return createRoom(request);
		case RequestCode::GetPlayersInRoom: return getPlayersInRoom(request);
		case RequestCode::JoinRoom: return joinRoom(request);
		case RequestCode::MyPerformances: return getPerformances(request);
		case RequestCode::BestScores: return getHighscores(request);
	}
}

void MenuRequestHandler::forceSignout()
{
	m_loginManager->logout(m_user.getUsername());
}

LoggedUser MenuRequestHandler::getUser()
{
	return m_user;
}

RequestResult *MenuRequestHandler::signout()
{
	RequestResult *result = new RequestResult;
	result->response = NULL;
	result->newHandler = m_handlerFactory->createLoginRequestHandler();

	m_loginManager->logout(m_user.getUsername());

	return result;
}

RequestResult *MenuRequestHandler::getRooms(Request request)
{
	RequestResult *result = new RequestResult;

	GetRoomsResponse getRoomsRes;
	
	//init getRooms response
	for (auto& it : m_roomManager->getRooms())
	{
		getRoomsRes.rooms[it.first] = it.second.getMetadata().name;
	}
	getRoomsRes.status = ResponseCode::Success;
	
	//serialize getRooms response
	char* serialRes = JsonResponsePacketSerializer::serializeResponse(getRoomsRes);

	//init result
	result->response = Communicator::createResponse(getRoomsRes.status, serialRes);
	result->newHandler = this;

	return result;
}

RequestResult *MenuRequestHandler::getPlayersInRoom(Request request)
{
	RequestResult *result = new RequestResult;

	GetPlayersInRoomResponse getPlayersRes;
	GetPlayersInRoomRequest getPlayersReq;
	Room *room = NULL;

	char* serialRes;
	try
	{
		getPlayersReq = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(request.buffer);
		room = m_roomManager->getRoom(getPlayersReq.roomId);
		getPlayersRes.status = ResponseCode::Success;
	}
	catch (...)
	{
		getPlayersRes.status = ResponseCode::InvalidRoomID;
	}

	if (getPlayersRes.status == ResponseCode::Success)
	{
		//init getPlayers response
		vector<LoggedUser> players = room->getAllUsers();
		for (LoggedUser &player : players)
		{
			getPlayersRes.players.push_back(player);
		}
	}
	//serialize getPlayers response
	serialRes = JsonResponsePacketSerializer::serializeResponse(getPlayersRes);

	//init result
	result->response = Communicator::createResponse(getPlayersRes.status, serialRes);
	result->newHandler = this;

	return result;
}

RequestResult *MenuRequestHandler::getHighscores(Request reqeust)
{
	RequestResult *result = new RequestResult;

	HighscoreResponse highscoreRes;
	 
	//init getHighscores response
	vector<Highscore> highscores = m_highscoreTable->getHighscores();
	for (Highscore highscore : highscores)
	{
		highscoreRes.highscores.push_back(highscore);
	}

	highscoreRes.status = ResponseCode::Success;

	//serialize getHighscores response
	char* serialRes = JsonResponsePacketSerializer::serializeResponse(highscoreRes);

	//init result
	result->response = Communicator::createResponse(highscoreRes.status, serialRes);
	result->newHandler = this;

	return result;
}

RequestResult *MenuRequestHandler::joinRoom(Request request)
{
	RequestResult *result = new RequestResult;

	JoinRoomResponse joinRoomRes;
	try
	{
		JoinRoomRequest joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request.buffer);

		// add user to the client.
		if ((joinRoomRes.status = m_roomManager->getRoom(joinRoomReq.roomId)->addUser(m_user)) == ResponseCode::Success)
		{
			delete result;
			result = new JoinLeaveRequestUpdate;
			
			Room *room = m_roomManager->getRoom(joinRoomReq.roomId);
			((JoinLeaveRequestUpdate *)result)->updateRoomType = ResponseCode::RoomPlayerLeaveJoin;
			((JoinLeaveRequestUpdate *)result)->playerName = m_user.getUsername();
			((JoinLeaveRequestUpdate *)result)->sockets = room->getAllSockets();
			
			result->newHandler = m_handlerFactory->createRoomMemberRequestHandler(m_user, m_roomManager->getRoom(joinRoomReq.roomId));
		}
		else
		{
			result->newHandler = this;
		}
	}
	catch (...)
	{
		joinRoomRes.status = ResponseCode::InvalidRoomID;
		result->newHandler = this;
	}

	//serialize getPlayers response
	char* serialRes = JsonResponsePacketSerializer::serializeResponse(joinRoomRes);

	//init result
	result->response = Communicator::createResponse(joinRoomRes.status, serialRes);

	return result;
}

RequestResult *MenuRequestHandler::createRoom(Request request)
{
	RequestResult *result = new RequestResult;

	CreateRoomResponse createRoomRes;
	CreateRoomRequest createRoomReq;

	try
	{
		createRoomReq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request.buffer);
		try
		{
			// createRoom might thow a fail ResponseCode.
			result->newHandler = m_handlerFactory->createRoomAdminRequestHandler(m_user, m_roomManager->createRoom(m_user, createRoomReq));
			createRoomRes.status = ResponseCode::Success;
		}
		catch (ResponseCode code)
		{
			createRoomRes.status = code;
			result->newHandler = this;
		}
	}
	catch (...)
	{
		createRoomRes.status = ResponseCode::InvalidCreateRoomArguementTypes;
		result->newHandler = this;
	}
	
	//serialize getPlayers response
	char* serialRes = JsonResponsePacketSerializer::serializeResponse(createRoomRes);

	//init result
	result->response = Communicator::createResponse(createRoomRes.status, serialRes);
	
	return result;
}

RequestResult *MenuRequestHandler::getPerformances(Request request)
{
	RequestResult *result = new RequestResult;
	GetPerformancesResponse performancesRes;

	try
	{
		performancesRes.gamesCount = m_database->getGameCountOfPlayer(m_user.getUsername());
		performancesRes.rightAnswersCount = m_database->getRightAnswerCountOfPlayer(m_user.getUsername());
		performancesRes.wrongAnswersCount = m_database->getWrongAnswersCountOfPlayer(m_user.getUsername());
		performancesRes.averageTimeForAnswer = m_database->getAvergaeTimeForAnswerOfPlayer(m_user.getUsername());
		performancesRes.status = ResponseCode::Success;
	}
	catch (...)
	{
		performancesRes.status = ResponseCode::UserNotFound;
	}

	char *serializedRes = JsonResponsePacketSerializer::serializeResponse(performancesRes);
	result->response = Communicator::createResponse(performancesRes.status, serializedRes);
	delete serializedRes;

	result->newHandler = this;

	return result;
}
