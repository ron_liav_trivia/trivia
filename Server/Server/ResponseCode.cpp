#include "ResponseCode.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include <string>

std::string ResponseMessages::getMessage(ResponseCode code)
{
	switch (code)
	{
		case ResponseCode::Error: return "something bad happened.";
		case ResponseCode::ProtocolFormat: return "invalid protocol format.";
		case ResponseCode::Success:	return "success.";
		case ResponseCode::UserTaken: return "username has already been taken.";
		case ResponseCode::EmailTaken: return "email has already been taken.";
		case ResponseCode::InvalidPasswordAndUsername: return "password and username does not match.";
		case ResponseCode::AlreadyLoggedIn: return "user is already logged in.";
		case ResponseCode::InvalidUsername: return "invalid username: the username must start with a letter and contains at least " + std::to_string(LoginManager::USER_MIN_LEN) + " letters.";
		case ResponseCode::InvalidPassword: return "invalid password: the password must has a capital, a letter, a digit and at least " + std::to_string(LoginManager::PASSWORD_MIN_LEN) + " letters.";
		case ResponseCode::InvalidEmail: return "invalid email format.";
		case ResponseCode::RoomFull: return "room is full.";
		case ResponseCode::RoomIsActive: return "the game has already started.";
		case ResponseCode::InvalidRoomID: return "the requested room couldn't be found.";
		case ResponseCode::AlreadyInRoom: return "this user is already in this room.";
		case ResponseCode::InvalidCreateRoomArguementTypes: return "invalid arguements' types";
		case ResponseCode::InvalidCreateRoomRoomName: return "invalid room name, the room name must starts with a letter and at least " + std::to_string(RoomManager::MIN_ROOM_NAME_LEN) + " characters";
		case ResponseCode::InvalidCreateRoomMaxUsers: return "invalid max players, the minimum max player for a room is " + std::to_string(RoomManager::MIN_USERS);
		case ResponseCode::InvalidCreateRoomQuestionCount: return "invalid number of questions, the minimum is " + std::to_string(RoomManager::MIN_QUESTION_COUNT);
		case ResponseCode::InvalidCreateRoomAnswerTimeout: return "invalid time for answer, the minimum is " + std::to_string(RoomManager::MIN_AMSWER_TIMEOUT) + " seconds";
		case ResponseCode::UserNotFound: return "the requested user could't be found";
		case ResponseCode::SqlInjection: return "are you serious? don't you dare to try that!";
		case ResponseCode::CantKickAdmin: return "you can't kick yourself...";
	}
	
	return "";
}
