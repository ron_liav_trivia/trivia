#pragma once
#include <vector>
#include <map>
#include "LoggedUser.h"
#include "RoomData.h"
#include "Room.h"
#include "Request.h"
#include "Sqlite.h"

using std::vector;
using std::map;

class RoomManager
{
public:
	static RoomManager *createObject(IDataBase*);
	static RoomManager *getObject();
	
	Room *createRoom(LoggedUser&, CreateRoomRequest);
	void deleteRoom(int ID);
	bool getRoomState(int ID);
	map<int, Room> getRooms();
	Room* getRoom(int ID);
	
	static const int MIN_ROOM_NAME_LEN = 3;
	static const int MIN_USERS = 1;
	static const int MIN_QUESTION_COUNT = 1;
	static const float MIN_AMSWER_TIMEOUT; // cant init here.
private:
	RoomManager(IDataBase*);

	static RoomManager *m_object;

	unsigned int m_nextRoomId;
	map<int, Room> m_rooms; //int refer to roomID
	IDataBase *m_database;
};