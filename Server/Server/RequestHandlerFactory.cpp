#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(LoginManager *loginManager, RoomManager* roomManager, GameManager* gameManager, HighscoreTable* highscoreTable, IDataBase *database) : m_loginManager(loginManager), m_roomManager(roomManager), m_gameManager(gameManager), m_highscoreTable(highscoreTable), m_database(database)
{
}

RequestHandlerFactory *RequestHandlerFactory::m_object = NULL;
RequestHandlerFactory *RequestHandlerFactory::createObject(LoginManager *loginManager, RoomManager* roomManager, GameManager* gameManager, HighscoreTable* highscoreTable, IDataBase *database)
{
	if (m_object)
	{
		throw std::exception("RequestHandlerFactory object has already been created");
	}

	m_object = new RequestHandlerFactory(loginManager, roomManager, gameManager, highscoreTable, database);
	return m_object;
}
RequestHandlerFactory * RequestHandlerFactory::getObject()
{
	if (!m_object)
	{
		throw std::exception("RequestHandlerFactory object must be created first.");
	}
	return m_object;
}

RequestHandlerFactory::~RequestHandlerFactory()
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(this, m_loginManager);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return new MenuRequestHandler(this, m_loginManager, m_roomManager, user, m_highscoreTable, m_database);
}

RoomAdminRequestHandler * RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room *room)
{
	return new RoomAdminRequestHandler(this, room, user, m_loginManager, m_roomManager);
}

RoomMemberRequestHandler * RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room *room)
{
	return new RoomMemberRequestHandler(this, room, user, m_loginManager, m_roomManager);
}

GameRequestHandler * RequestHandlerFactory::createGameRequestHandler(LoggedUser user, Room *room)
{
	return new GameRequestHandler(this, room, m_gameManager, m_loginManager, m_database, user);
}

GameRequestHandler * RequestHandlerFactory::createGameRequestHandler(LoggedUser user, int gameId)
{
	return new GameRequestHandler(this, gameId, m_gameManager, m_loginManager, m_database, user);
}
