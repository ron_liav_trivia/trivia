#include "LoginRequestHandler.h"
#include "Request.h"
#include "ResponseCode.h"
#include "RequestHandlerFactory.h"
#include "Communicator.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory *handlerFactory, LoginManager *loginManager) : m_handlerFactory(handlerFactory), m_loginManager(loginManager) {}

LoginRequestHandler::~LoginRequestHandler()
{
}

bool LoginRequestHandler::isRequestRelevant(Request request)
{
	return (request.id == RequestCode::Login || request.id == RequestCode::Signup) && request.buffer != NULL;
}

RequestResult *LoginRequestHandler::handleRequest(Request request)
{
	return request.id == RequestCode::Login ? login(request) : signup(request);
}


RequestResult *LoginRequestHandler::login(Request request)
{
	RequestResult *res = new RequestResult;
	LoginResponse loginRes;
	LoginRequest loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer);
	char *serializedRes;
	
	loginRes.status = m_loginManager->login(loginReq.username, loginReq.password);

	if (loginRes.status == ResponseCode::Success)
	{
		res->newHandler = m_handlerFactory->createMenuRequestHandler(loginReq.username);
	}
	else
	{
		res->newHandler = this;
	}
	
	serializedRes = JsonResponsePacketSerializer::serializeResponse(loginRes);
	res->response = Communicator::createResponse(loginRes.status, serializedRes);
	delete serializedRes;
	
	return res;
}

RequestResult *LoginRequestHandler::signup(Request request)
{
	RequestResult *res = new RequestResult;
	SignUpResponse signUpRes;
	SignUpRequest signUpReq = JsonRequestPacketDeserializer::deserializeSignUpRequest(request.buffer);
	
	signUpRes.status = m_loginManager->signup(signUpReq.username, signUpReq.email, signUpReq.password);
	
	char *serializedRes = JsonResponsePacketSerializer::serializeResponse(signUpRes);
	res->response = Communicator::createResponse(signUpRes.status, serializedRes);
	delete serializedRes;
	
	// signup doesn`t change the user's state.
	res->newHandler = this;
	
	return res;
}