﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Timers;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace client
{
    [Activity(Label = "GameWindow")]
    public class GameWindow : Activity
    {
        RoomMetaData room;
        TextView roomNameView, questionNumberView,timeleftView, questionView;
        Button leaveGame;
        Handler ui;
        TextView waitingProgressBar;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_game);
            ui = new Handler();
            room = Client.GetClient().GetCurrentRoom();
            
            roomNameView = FindViewById<TextView>(Resource.Id.game_roomName);
            questionNumberView = FindViewById<TextView>(Resource.Id.game_questionNumber);
            timeleftView = FindViewById<TextView>(Resource.Id.game_timeleft);
            questionView = FindViewById<TextView>(Resource.Id.game_question);
            waitingProgressBar = FindViewById<TextView>(Resource.Id.game_waitingMessage);
            leaveGame = FindViewById<Button>(Resource.Id.game_leave);
            leaveGame.Click += delegate
            {
                Client.GetClient().LeaveGame();
                LoadingWindow.ShowWindow<GameResults>(this, true);
            };

            roomNameView.Text = "Game:\n" + room.name;
            answerTimeout = room.answerTimeout;
            questionsLeft = room.questionCount;

            answerButtons = new Button[ANSWER_BUTTONS_COUNT] {
                FindViewById<Button>(Resource.Id.game_answer1),
                FindViewById<Button>(Resource.Id.game_answer2),
                FindViewById<Button>(Resource.Id.game_answer3),
                FindViewById<Button>(Resource.Id.game_answer4)
            };
            for (int i = 0; i < answerButtons.Length; i++)
            {
                int answerId = i; // don't touch this!
                answerButtons[i].Click += delegate { SubmitAnswer(answerId); };
            }

            timer = new System.Timers.Timer(1000);
            timer.Elapsed += delegate
            {
                ui.Post(() =>
                {
                    timeleftView.Text = "Timeleft:\n" + questionTimeleft;
                    if (questionTimeleft == 0)
                    {
                        SubmitAnswer(NO_ANSWER_ID);
                    }
                    questionTimeleft--;
                });
            };
            
            nextQuestionWorker = new BackgroundWorker();
            nextQuestionWorker.DoWork += delegate { NextQuestion(); };

            getGameResultsWorker = new BackgroundWorker();
            getGameResultsWorker.DoWork += delegate
            {
                GetGameResultsResult gameResults = Client.GetClient().GetGameResults();
                ui.Post(delegate
                {
                    LoadingWindow.ShowWindow<GameResults>(this, true);
                });
            };

            Task.Run(() => StartGame());
        }

        private void StartGame()
        {
            while (questionsLeft > 0)
            {
                nextQuestionWorker.RunWorkerAsync();
                WaitForPlayers(nextQuestionWorker);

                questionTimeleft = (int)answerTimeout;
                timer.Start();
                

                isQuestionSubmitted = false;
                while (!isQuestionSubmitted && timer.Enabled)
                {
                    // wait for submit or timer elapsed.
                }

                Thread.Sleep(QUESTION_DELAY);
            }

            getGameResultsWorker.RunWorkerAsync();
            WaitForPlayers(getGameResultsWorker);
        }

        private void WaitForPlayers(BackgroundWorker worker)
        {
            Thread.Sleep(LOADING_SCREEN_DELAY);
            if (worker.IsBusy)
            {
                ui.Post(() => 
                {
                    waitingProgressBar.Visibility = ViewStates.Visible;
                    questionView.Visibility = ViewStates.Gone;
                });
            }
            while (worker.IsBusy)
            {
                // wait...
            }
            ui.Post(() => 
            {
                waitingProgressBar.Visibility = ViewStates.Gone;
                questionView.Visibility = ViewStates.Visible;
            });
        }

        private void SubmitAnswer(int answerId)
        {
            if (!isQuestionSubmitted)
            {
                SubmitAnswerResult submitAnswerRes = Client.GetClient().SubmitAnswer(answerId);
                if (submitAnswerRes.success)
                {
                    isQuestionSubmitted = true;
                    foreach (Button answerButton in answerButtons)
                    {
                        answerButton.Clickable = false;
                        answerButton.SetBackgroundColor(OTHER_ANSWER_COLOR);
                    }
                    if (answerId != NO_ANSWER_ID)
                    {
                        answerButtons[answerId].SetBackgroundColor(WRONG_ANSWER_COLOR);
                        answerButtons[submitAnswerRes.correctAnswer].SetBackgroundColor(CORRECT_ANSWER_COLOR);
                    }
                    questionsLeft--;
                    timer.Stop();
                }
            }
        }

        private void NextQuestion()
        {
            //get question result from the server
            GetQuestionResult questionRes;

            do
            {
                questionRes = Client.GetClient().GetNextQuestion();
            } while (!questionRes.success);

            if (questionRes.success)
            {
                ui.Post(delegate
                {
                    //init questionsLeft
                    questionNumberView.Text = "Question:\n" + Convert.ToString(questionsLeft);
                    timeleftView.Text = "Timeleft:\n" + Convert.ToString(answerTimeout);

                    //init question;
                    questionView.Text = questionRes.question.question;

                    //init answers
                    for (int i = 0; i < answerButtons.Length; ++i)
                    {
                        answerButtons[i].Clickable = true;
                        answerButtons[i].Text = questionRes.question.answers[i];
                        answerButtons[i].SetBackgroundColor(DEFAULT_ANSWER_COLOR[i]);
                    }
                });
            }
        }

        private const int NO_ANSWER_ID = -1;
        private const int QUESTION_DELAY = 1000;
        private const int LOADING_SCREEN_DELAY = 100;
        private const int ANSWER_BUTTONS_COUNT = 4;
        private readonly Color CORRECT_ANSWER_COLOR = Color.Green;
        private readonly Color WRONG_ANSWER_COLOR = Color.Red;
        private readonly Color OTHER_ANSWER_COLOR = Color.LightGray;
        private readonly Color[] DEFAULT_ANSWER_COLOR = { Color.Green, Color.Blue, Color.Orange, Color.Magenta };
        
        private BackgroundWorker nextQuestionWorker;
        private BackgroundWorker getGameResultsWorker;

        private System.Timers.Timer timer;
        private int questionTimeleft;
        private Button[] answerButtons;
        private int questionsLeft;
        private double answerTimeout;
        private bool isQuestionSubmitted;
    }
}