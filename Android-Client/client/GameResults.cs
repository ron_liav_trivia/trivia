﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace client
{
    [Activity(Label = "GameResults")]
    public class GameResults : Activity
    {
        Button back;
        TextView resultsView;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_gameResults);
            back = FindViewById<Button>(Resource.Id.gameResults_back);
            back.Click += delegate { Client.GetClient().LeaveGame(); LoadingWindow.ShowWindow<MainWindow>(this, true); };

            resultsView = FindViewById<TextView>(Resource.Id.gameResults_resultsView);
            GetGameResultsResult gameResults = Client.GetClient().GetGameResults();
            for (int i = 0; i < gameResults.playerResults.Count(); i++)
            {
                resultsView.Text += String.Format("{0} - {1}\n", gameResults.playerResults[i].username, gameResults.playerResults[i].score);
            }
        }
    }
}