﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Text;
using Android.Text.Style;
using Android.Graphics;

namespace client
{
    [Activity(Label = "RoomWindow")]
    public class RoomWindow : Activity
    {
        TextView roomInfo;
        TextView errorMessage;
        Button startGame, closeRoom, leaveRoom, kickPlayer;
        bool isAdmin;

        Task updateRoomTask;
        bool isUpdateRoomCancelled, isUpdateRoomBlocked;
        Handler ui;

        LinearLayout playersPanel;
        Dictionary<string, TextView> players;
        string playerToKick;

        TextView chat;
        EditText inputMsg;
        Button sendMsg;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_room);
            
            roomInfo = FindViewById<TextView>(Resource.Id.room_info);
            
            playersPanel = FindViewById<LinearLayout>(Resource.Id.room_playersPanel);

            errorMessage = FindViewById<TextView>(Resource.Id.room_errorMessage);

            chat = FindViewById<TextView>(Resource.Id.room_chat);
            inputMsg = FindViewById<EditText>(Resource.Id.room_inputMsg);
            sendMsg = FindViewById<Button>(Resource.Id.room_sendMsg);
            sendMsg.Click += SendButtonOnClick;

            startGame = FindViewById<Button>(Resource.Id.room_start);
            kickPlayer = FindViewById<Button>(Resource.Id.room_kick);
            closeRoom = FindViewById<Button>(Resource.Id.room_close);
            leaveRoom = FindViewById<Button>(Resource.Id.room_leave);

            startGame.Click += StartGameOnClick;
            kickPlayer.Click += KickPlayerOnClick;
            closeRoom.Click += LeaveRoomOnClick;
            leaveRoom.Click += LeaveRoomOnClick;

            GetRoomInfo();

            if (isAdmin = Client.GetClient().IsRoomAdmin())
            {
                startGame.Visibility = ViewStates.Visible;
                kickPlayer.Visibility = ViewStates.Visible;
                closeRoom.Visibility = ViewStates.Visible;
            }
            else
            {
                leaveRoom.Visibility = ViewStates.Visible;
            }

            CreateChatMessage("You have joined the room.");
            ui = new Handler();
            updateRoomTask = Task.Run(() => UpdateRoom());
        }

        private void UpdateRoom()
        {
            isUpdateRoomCancelled = false;

            while (!isUpdateRoomCancelled)
            {
                RoomUpdateResult roomUpdateResult;
                try { roomUpdateResult = Client.GetClient().GetRoomUpdate(ref isUpdateRoomCancelled, ref isUpdateRoomBlocked); }
                catch { return; }

                switch (roomUpdateResult.updateType)
                {
                    case UpdateRoomType.CloseRoom:
                    {
                        Task.Run(() => ui.Post(() => { ChangeWindow(false, true); }));
                        break;
                    }
                    case UpdateRoomType.StartGame:
                    {
                        Task.Run(() => ui.Post(() => { ChangeWindow(true, false); }));
                        break;
                    }
                    case UpdateRoomType.KickPlayer:
                    {
                        if (Client.GetClient().GetUsername() == roomUpdateResult.playerName)
                        {
                            ui.Post(() =>
                            {
                                Android.App.AlertDialog.Builder dialog = new Android.App.AlertDialog.Builder(this);
                                Android.App.AlertDialog alert = dialog.Create();
                                alert.SetTitle("Error");
                                alert.SetMessage("You have been kicked from the room.");
                                alert.SetIcon(Resource.Drawable.error);
                                alert.SetButton("OK", (c, ev) => { ChangeWindow(false, true); });
                                alert.SetCanceledOnTouchOutside(false);
                                alert.Show();
                            });
                        }
                        else
                        {
                            ui.Post(() =>
                            {
                                playersPanel.RemoveView(players[roomUpdateResult.playerName]);
                                players.Remove(roomUpdateResult.playerName);
                                CreateChatMessage(roomUpdateResult.playerName + " has been kicked.");
                            });
                        }
                        break;
                    }
                    case UpdateRoomType.LeaveJoin:
                    {
                        if (players.ContainsKey(roomUpdateResult.playerName))
                        {
                            ui.Post(() =>
                            {
                                playersPanel.RemoveView(players[roomUpdateResult.playerName]);
                                players.Remove(roomUpdateResult.playerName);
                                CreateChatMessage(roomUpdateResult.playerName + " has left the room.");
                            });
                        }
                        else
                        {
                            ui.Post(() =>
                            {
                                TextView player = CreateNewPlayerTextView(roomUpdateResult.playerName);
                                players[roomUpdateResult.playerName] = player;
                                string name = roomUpdateResult.playerName;
                                player.Click += delegate { ChoosePlayerToKick(name); };
                                playersPanel.AddView(player);
                                CreateChatMessage(roomUpdateResult.playerName + " has joined the room.");
                            });
                        }
                        break;
                    }
                    case UpdateRoomType.NewMessage:
                    {
                        ui.Post(() => CreateChatMessage(roomUpdateResult.message, roomUpdateResult.playerName));
                        break;
                    }
                }
            }
        }

        private TextView CreateNewPlayerTextView(string content)
        {
            TextView player = new TextView(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MatchParent, LinearLayout.LayoutParams.WrapContent);
            layoutParams.SetMargins(0, 5, 0, 0);
            player.LayoutParameters = layoutParams;
            player.SetBackgroundColor(Color.ParseColor("#FF222222"));
            player.SetTextColor(Color.ParseColor("#FFFFBB33"));
            player.Gravity = GravityFlags.CenterVertical;
            player.TextSize = 20;
            player.Text = content;
            player.SetPadding(10, 10, 0, 10);
            return player;
        }

        private void CreateChatMessage(string message, string playerName = null)
        {
            if (playerName == null)
            {
                playerName = "System";
            }
            var s = new SpannableString(playerName + ": ");
            s.SetSpan(new StyleSpan(TypefaceStyle.Bold), 0 , playerName.Length + 1, 0);
            chat.Text += s + message + "\n";
        }
        private void ChangeWindow(bool isStart, bool isLeave)
        {
            isUpdateRoomCancelled = true;
            while (!updateRoomTask.IsCompleted)
            {
                // wait...
            }

            if (isStart) LoadingWindow.ShowWindow<GameWindow>(this, true);
            else if (isLeave) LoadingWindow.ShowWindow<MainWindow>(this, true);
        }

        private void GetRoomInfo()
        {
            GetRoomInfoResult getRoomInfoResult = Client.GetClient().GetRoomInfo();

            if (getRoomInfoResult.success)
            {
                roomInfo.Text = getRoomInfoResult.room.name;

                players = new Dictionary<string, TextView>();
                foreach (string playerName in getRoomInfoResult.room.players)
                {
                    TextView playerView = CreateNewPlayerTextView(playerName);
                    players[playerName] = playerView;
                    if (isAdmin)
                    {
                        string name = playerName;
                        playerView.Click += delegate { ChoosePlayerToKick(name); };
                    }
                    playersPanel.AddView(playerView);
                }
            }
            else
            {
                LoadingWindow.ShowAlert(this, getRoomInfoResult.errorMessage);
            }
        }

        public void LeaveRoomOnClick(object o, EventArgs e)
        {
            isUpdateRoomBlocked = true;
            Client.GetClient().LeaveRoom();
            ChangeWindow(false, true);
            isUpdateRoomBlocked = false;
        }

        public void StartGameOnClick(object o, EventArgs e)
        {
            isUpdateRoomBlocked = true;
            Client.GetClient().StartGame();
            ChangeWindow(true, false);
            isUpdateRoomBlocked = false;
        }

        private void ChoosePlayerToKick(string playerName)
        {
            if (playerToKick != null)
            {
                players[playerToKick].SetBackgroundColor(Color.White);
            }
            playerToKick = playerName;
            players[playerToKick].SetBackgroundColor(Color.Blue);
        }
        private void KickPlayerOnClick(object o, EventArgs e)
        {
            isUpdateRoomBlocked = true;
            KickPlayerResult res = Client.GetClient().KickPlayer(playerToKick);

            if (players.ContainsKey(playerToKick))
            {
                players[playerToKick].SetBackgroundColor(Color.ParseColor("#FF222222"));
            }
            if (!res.success)
            {
                LoadingWindow.ShowAlert(this, res.errorMessage);
            }
            playerToKick = null;
            isUpdateRoomBlocked = false;
        }
        private void SendButtonOnClick(object o, EventArgs e)
        {
            if (inputMsg.Text != null)
            {
                Client.GetClient().SendMessage(inputMsg.Text);
                inputMsg.Text = "";
            }
        }
    }
}